package banco.bancomaven;

import java.util.Date;

public class CuentaBancaria {

	private static int idCuenta;
	private Cliente cliente;
	private Date fechaIngreso;
	private float saldoActual;
	private String estado;
	private float limiteExt;
	
	
	public CuentaBancaria() {
		
	}
	public CuentaBancaria (Cliente cliente, float saldo, float limiteExt, String estado, Date fechaIngreso) {
		idCuenta = idCuenta+1;
		this.saldoActual = saldo;
		this.limiteExt = limiteExt;
		this.cliente = cliente;
		this.estado = estado;
		this.fechaIngreso = fechaIngreso;
		
	}
	
	
	public int getIdCuenta() {
		return idCuenta;
	}

	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public float getSaldoActual() {
		return saldoActual;
	}
	public void setSaldoActual(float saldoActual) {
		this.saldoActual = saldoActual;
	}
	public String isEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public float getLimiteExt() {
		return limiteExt;
	}
	public void setLimiteExt(float limiteExt) {
		this.limiteExt = limiteExt;
	}
	
}
