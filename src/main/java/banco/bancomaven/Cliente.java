package banco.bancomaven;

import java.util.ArrayList;


public class Cliente {
	
	private static int idCliente;
	private int dni;
	private String nombre;
	private String correo;
	private String domicilio;
	private String estado;
	private ArrayList<Cliente> adherentes;
	private Cliente adherenteDe;
	private CuentaBancaria cuenta;

	public Cliente(){
		
	}
	
	public Cliente (int dni,String nombre, CuentaBancaria cuenta, String correo, String domicilio, String estado, ArrayList<Cliente> adherentes ) {
		this.dni = dni;
		this.nombre = nombre;
		this.cuenta = cuenta;
		idCliente = idCliente+1;
		this.correo = correo;
		this.domicilio = domicilio;
		this.estado = estado;
		this.adherentes = adherentes;
	}
	
	public Cliente (int dni,String nombre, CuentaBancaria cuenta, String correo, String domicilio, String estado, Cliente adherenteDe ) {
		this.dni = dni;
		this.nombre = nombre;
		this.cuenta = cuenta;
		idCliente = idCliente+1;
		this.correo = correo;
		this.domicilio = domicilio;
		this.estado = estado;
		this.adherenteDe = adherenteDe;

	}
	
	public int getIdCliente () {
		return idCliente;
	}
	
	public int getDni () {
		return dni;
	}
	public void setDni (int val) {
		this.dni = val;
	}
	
	public String getNombre () {
		return nombre;
	}
	public void setNombre (String val) {
		this.nombre = val;
	}
	
	public String getCorreo () {
		return correo;
	}
	public void setCorreo (String val) {
		this.correo = val;
	}
	
	public String getDomicilio () {
		return domicilio;
	}
	public void setDomicilio (String val) {
		this.domicilio = val;
	}
	
	public String getEstado () {
		return estado;
	}
	public void setEstado (String val) {
		this.estado = val;
	}

	public ArrayList<Cliente> getAdherentes() {
		return adherentes;
	}

	public void setAdherentes(ArrayList<Cliente> adherentes) {
		this.adherentes = adherentes;
	}

	public Cliente getAdherenteDe() {
		return adherenteDe;
	}

	public void setAdherenteDe(Cliente adherenteDe) {
		this.adherenteDe = adherenteDe;
	}
	
	public CuentaBancaria getCuenta() {
		return cuenta;
	}

	public void setCuenta(CuentaBancaria cuenta) {
		this.cuenta = cuenta;
	}

}
