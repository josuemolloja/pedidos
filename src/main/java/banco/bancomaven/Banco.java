package banco.bancomaven;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Banco {
		
	//Crear cuentas bancarias
		private List<CuentaBancaria> listacuentas;
		private List<Cliente> listaClientes;
		private List<Movimiento> listaMovimientos;
		private Movimiento movimiento;
		private Date fecha;
		
		public Banco(){
			listacuentas = new ArrayList<CuentaBancaria>();
			listaClientes = new ArrayList<Cliente>();
			listaMovimientos = new ArrayList<Movimiento>();
		}
		
		
		public void agregarCuenta(CuentaBancaria cuenta){
			listacuentas.add(cuenta);		
		}
			
		public List<CuentaBancaria> getListacuentas() {
			return listacuentas;
		}

		public void setListacuentas(List<CuentaBancaria> listacuentas) {
			this.listacuentas = listacuentas;
		}
				
		
		
		public void agregarCliente(Cliente cliente){
			listaClientes.add(cliente);		
		}
			
		public List<Cliente> getListaClientes() {
			return listaClientes;
		}

		public void setListaClientes(List<Cliente> listaClientes) {
			this.listaClientes = listaClientes;
		}
		
		public void extraer (float importe, CuentaBancaria cuenta) {
		if (cuenta.getSaldoActual()>=importe && cuenta.getLimiteExt()>=importe) {
			fecha = new Date();
			movimiento = new Movimiento( cuenta, importe, fecha);
			listaMovimientos.add(movimiento);
				cuenta.setSaldoActual(cuenta.getSaldoActual()-importe);
			}
		}
		
		public void agregar (float importe, CuentaBancaria cuenta) {
			fecha = new Date();
			movimiento = new Movimiento( cuenta, importe, fecha);
			listaMovimientos.add(movimiento);
			cuenta.setSaldoActual(cuenta.getSaldoActual()+importe);	
		}
		
		

}
