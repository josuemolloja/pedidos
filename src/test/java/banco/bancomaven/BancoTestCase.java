package banco.bancomaven;

import java.util.Date;

//import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class BancoTestCase extends TestCase {
Banco target;
	
	@Before
	public void setUp() throws Exception {
		target = new Banco();
	}
	
	@After
	public void tearDown() throws Exception {
		target = null;
	}

	@Test
	public void testCrearCuenta() {
		Cliente cliente = new Cliente();
		Date fecha = new Date();
		CuentaBancaria cuenta = new CuentaBancaria(cliente,20000,3000,"habilitado",fecha);
		target.agregarCuenta(cuenta);
		assertTrue(target.getListacuentas().get(0).getSaldoActual()== 20000);

		//assertTrue(target.getListacuentas().size() == 1);
	}
	
	@Test
	public void testAgregar() {
		Cliente cliente = new Cliente();
		Date fecha = new Date();
		CuentaBancaria cuenta = new CuentaBancaria(cliente,20000,3000,"habilitado",fecha);
		target.agregarCuenta(cuenta);
		target.agregar(2000,cuenta);
		assertTrue(target.getListacuentas().get(0).getSaldoActual() == 22000 );
	}
	
	@Test
	public void testExtraer() {
		Cliente cliente = new Cliente();
		Date fecha = new Date();
		CuentaBancaria cuenta = new CuentaBancaria(cliente,20000,3000,"habilitado",fecha);
		target.agregarCuenta(cuenta);
		target.extraer(2000,cuenta);
		assertTrue(target.getListacuentas().get(0).getSaldoActual() == 18000);
	}
	

}
